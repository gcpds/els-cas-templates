


\newenvironment{Keywords}
{
\cleardoublepage
\setsinglecolumn
\vspace*{0.2\textheight}
\thispagestyle{empty}
\centering
\itshape
}


\newenvironment{Graphical Abstract}
{
\cleardoublepage
\setsinglecolumn
\vspace*{0.2\textheight}
\thispagestyle{empty}
\centering
\itshape
}


\newenvironment{Highlights}{
        \begin{list}{*}{}
}{
        \end{list}
}


